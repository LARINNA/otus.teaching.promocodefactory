﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Procedures
{
    public class CustomerProcedure
    {
        public static Customer MakeCustomer(CreateOrEditCustomerRequest request, IEnumerable<Preference> preferences, Customer _customer = null) 
        {
            List<CustomerPreference> customerPreferences = new List<CustomerPreference>();
            Guid customerId = Guid.NewGuid();
            foreach (var preference in preferences)
            {
                customerPreferences.Add(new CustomerPreference() { CustomerId = customerId, PreferenceId = preference.Id });
            }
             
            if (_customer == null)
            {
                _customer = new Customer();
                _customer.Id = Guid.NewGuid();
            }


            _customer.FirstName = request.FirstName;
            _customer.LastName = request.LastName;
            _customer.Email = request.Email;
            _customer.Preferences = customerPreferences;
           
            return _customer;
        }
    }
}
