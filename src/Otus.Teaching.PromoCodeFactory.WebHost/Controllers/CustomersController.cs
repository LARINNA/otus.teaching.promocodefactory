﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Procedures;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
       private readonly DataContext _context;
        public CustomersController(IRepository<Customer> customerRepository,
                                   IRepository<Preference> preferenceRepository, IRepository<PromoCode> promocodeRepository,
                                   DataContext context)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promocodeRepository = promocodeRepository;
            _context = context; 
        }
        /// <summary>
        /// Получить данные всех клиентов
        ///  </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                }).ToList();

            return Ok(customersModelList);
        }
        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) { return NotFound(); }

            var preferences = _context.Preferences.FromSqlInterpolated($"Select p.* from Preferences p join CustomerPreference cp on p.Id = cp.PreferenceId and cp.CustomerId = {id}").ToList();
            CustomerResponse customerResponce = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.Promocodes.Select(p=> new PromoCodeShortResponse() 
                {
                    Id= p.Id,   
                    Code = p.Code,  
                    ServiceInfo= p.ServiceInfo, 
                    BeginDate=p.BeginDate.ToShortDateString(),
                    EndDate = p.EndDate.ToShortDateString(),
                    PartnerName = p.PartnerName
                }).ToList(),
                Preferences = preferences
            };
            return Ok(customerResponce);
        }
        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<List<Customer>>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);
            Customer customer = CustomerProcedure.MakeCustomer(request, preferences);
            await _customerRepository.CreateAsync(customer);
            var customers = await _customerRepository.GetAllAsync();
           return Ok(customers.ToList());

        }


        /// <summary>
        /// Отредактировать данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            customer = CustomerProcedure.MakeCustomer(request, preferences, customer);
            await _customerRepository.UpdateAsync(customer);
            var customers = await _customerRepository. GetAllAsync();
           return Ok(customers.ToList());
        }
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<CustomerShortResponse>> DeleteCustomerAsync(Guid id)
        {
             var customers = await _customerRepository.GetAllAsync();
            var customer = customers.FirstOrDefault(c=>c.Id==id);
            var promocodes = await _promocodeRepository.GetAllAsync();
            var promocodeForCustomer = promocodes.Where(p => p.Customer.Id == id).ToList();
            foreach (var promocode in promocodeForCustomer)
            {
                await _promocodeRepository.DeleteAsync(promocode.Id);
            }
            await _customerRepository.DeleteAsync(id);
            return await this.GetCustomersAsync();
        }
    }
}