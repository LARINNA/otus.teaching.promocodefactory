﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public DbSet<PromoCode> Promocodes { get; set; }

         public DataContext()
        {

        }
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
           // Database.EnsureDeleted();
            /*Database.EnsureCreated();
            this.Customers.AddRange(FakeDataFactory.Customers);
            this.SaveChanges();
            this.Preferences.AddRange(FakeDataFactory.Preferences);
            this.SaveChanges();
            this.Roles.AddRange(FakeDataFactory.Roles);
            this.SaveChanges();
            foreach (Employee employee in FakeDataFactory.Employees)
            {
                var role = Roles.Find(employee.Role.Id);
                if (role != null) 
                {
                    var newEmployee = new Employee()
                    { 
                        Id = Guid.NewGuid(),
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,   
                        Email = employee.Email,
                        AppliedPromocodesCount= employee.AppliedPromocodesCount,    
                        Role = role
                    };
                    this.Employees.Add(newEmployee);
                }
            }
            this.SaveChanges();*/
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .Property(p => p.FirstName)
                .HasMaxLength(50);
            modelBuilder.Entity<Customer>()
               .Property(p => p.LastName)
               .HasMaxLength(50);
            modelBuilder.Entity<Customer>()
               .Property(p => p.Email)
               .HasMaxLength(50);

            modelBuilder.Entity<Preference>()
               .Property(p => p.Name)
               .HasMaxLength(50);

            modelBuilder.Entity<PromoCode>()
               .Property(p => p.Code)
               .HasMaxLength(50);
            modelBuilder.Entity<PromoCode>()
               .Property(p => p.ServiceInfo)
               .HasMaxLength(50);
            modelBuilder.Entity<PromoCode>()
               .Property(p => p.PartnerName)
               .HasMaxLength(50);

            modelBuilder.Entity<Employee>()
               .Property(p => p.FirstName)
               .HasMaxLength(50);
            modelBuilder.Entity<Employee>()
               .Property(p => p.LastName)
               .HasMaxLength(50);
            modelBuilder.Entity<Employee>()
               .Property(p => p.Email)
               .HasMaxLength(50);

            modelBuilder.Entity<Role>()
               .Property(p => p.Name)
               .HasMaxLength(50);
            modelBuilder.Entity<Role>()
               .Property(p => p.Description)
               .HasMaxLength(100);

            modelBuilder.Entity<CustomerPreference>()
                 .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId);
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Customer)
                .WithMany(b => b.Promocodes);
            modelBuilder.Entity<Role>()
                .HasKey(r => r.Id);
            modelBuilder.Entity<Employee>()
                .HasOne(r => r.Role)
                .WithMany();
        }
    }
    
}
