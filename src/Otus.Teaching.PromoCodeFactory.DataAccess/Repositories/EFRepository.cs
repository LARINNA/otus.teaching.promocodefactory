﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly DataContext _context;
        public EfRepository(DataContext context)
        {
            _context = context;
        }


        public async Task<IEnumerable<T>> GetAllAsync()
        {
              return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _context.Set<T>().FirstOrDefaultAsync(i => i.Id == id);
        }
        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _context.Set<T>().AsNoTracking().Where(x => ids.Contains(x.Id)).ToListAsync();
            return entities;

        }

        public async Task  CreateAsync(T t)
        {
            await _context.Set<T>().AddAsync(t);
            await _context.SaveChangesAsync();
        }
        public async Task  UpdateAsync(T t)
        {
            await this.DeleteAsync(t.Id);
            await this.CreateAsync(t);
        }

        public async Task DeleteAsync(Guid id)
        {
            T tDeleted = await _context.Set<T>().FindAsync( id);
            _context.Set<T>().Remove(tDeleted);
           await _context.SaveChangesAsync();
        }
    }
}
