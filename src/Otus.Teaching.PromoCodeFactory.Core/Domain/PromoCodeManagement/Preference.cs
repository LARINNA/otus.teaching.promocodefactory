﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
    }
}